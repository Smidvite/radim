import numpy
import math
class Node:
    def __init__(self, leftN, rightN,val):
        self.leftN = leftN
        self.rightN = rightN
        self.val = val

    def getLeft(self):
        return self.leftN

    def getRight(self):
        return self.rightN

    def getVal(self):
        return self.val 

    def setLeft(self, val):
        self.leftN = val

    def setRight(self, val):
        self.rightN = val

    def setVal(self,val):
        self.val = val


class Tr:
    nodes = []
    deletedVals = []

    def __init__(self, rootNode):
        self.rootNode = rootNode
        self.nodes.append(rootNode)
    
    def checkIfThere(self,oldNode,node):
        if(node.getVal() == oldNode.getVal()):
            return True

        if(node.getVal()>oldNode.getVal()):
            n = oldNode.getRight()
            if(n != None):
                if(self.checkIfThere(oldNode.getRight(),node)):
                    return True
        else:
            n = oldNode.getLeft()
            if(n != None):
                if(self.checkIfThere(oldNode.getLeft(),node)):
                    return True

        return False

    def getNodes(self):
        return self.nodes

    def addNodeRec(self, node,newValue):
        if(newValue > node.getVal() or newValue == node.getVal()):
            if(self.canGoRight(node)):
                biggerNode = self.goRight(node)
                self.addNodeRec(biggerNode,newValue)
            else:
                n = Node(None, None, newValue)
                node.setRight(n)
                self.nodes.append(n)
        else:
            if(self.canGoLeft(node)):
                smallerNode = self.goLeft(node)
                self.addNodeRec(smallerNode,newValue)
            else:
                n = Node(None, None, newValue)
                node.setLeft(n)
                self.nodes.append(n)

    def addNode(self, newValue):
        self.addNodeRec(self.rootNode,newValue)

    def canGoRight(self, node):
        if(node.getRight() != None):
            return True
        else:
            return False

    def goRight(self, node):
        return node.getRight()

    def defGoRightTillEnd(self, node):
        if(self.canGoRight(node)):
            node = self.goRight(node)
            self.defGoRightTillEnd(node)
        return n

    def canGoLeft(self,node):
        if(node.getLeft() != None):
            return True
        else:
            return False

    def goLeft(self, node):
        return node.getLeft()

    def calculateHightOfAllThree(self):
        self.branchLength = 0
        self.branchWidth = 0
        self.ctr = 0
        self.lastItNodeRigth = None
        self.lastItNodeLeft = None

        self.branchLengths = []
        self.branchWidths = []
        self.delVals = []
        self.calculateHightReal(self.rootNode)
        print("Lengths are:",end='')
        print(self.branchLengths)
        print("Widhts are:",end='')
        print(self.branchWidths)
        print(self.ctr)

    def calculateLeftHight(self):
        pass

    def calculateRigthHight(self):
        self.branchLength = 0
        self.branchWidth = 0
        self.ctr = 0
        self.lastItNodeRigth = None
        self.lastItNodeLeft = None

        self.branchLengths = []
        self.branchWidths = []

        self.calcForNode = self.rootNode.getRight()
        self.calculateHightReal(self.calcForNode)
        return max(self.branchLengths)

    def calculateLeftHight(self):
        self.branchLength = 0
        self.branchWidth = 0
        self.ctr = 0
        self.lastItNodeRigth = None
        self.lastItNodeLeft = None

        self.branchLengths = []
        self.branchWidths = []

        self.calcForNode = self.rootNode.getLeft()
        self.calculateHightReal(self.calcForNode)
        return max(self.branchLengths)

    def calculatePosMatrix(self):
        self.branchLength = 0
        self.branchWidth = 0
        self.ctr = 0
        self.lastItNodeRigth = None
        self.lastItNodeLeft = None

        self.branchLengths = []
        self.branchWidths = []

        self.calcForNode = self.rootNode.getLeft()
        self.calculateHightReal(self.rootNode)
        self.deletedVals.append(self.rootNode.getVal())
        minValWid = min(self.branchWidths)
        self.branchWidths = [x-minValWid for x in self.branchWidths]


        self.posMatrix = numpy.full((max(self.branchLengths)+1,max(self.branchWidths)+1),math.inf)

        for x in range(len(self.branchLengths)):
            self.posMatrix[self.branchLengths[x],self.branchWidths[x]] = self.deletedVals[x]

        for i in range(self.posMatrix.shape[0]):
            print("")
            for j in range(self.posMatrix.shape[1]):
                if(self.posMatrix[i][j] != math.inf):
                    print(int(self.posMatrix[i][j]),end='')
                else:
                    print("  ",end='')
        print("")
        return self.posMatrix

    def calculateWidth(self):
        self.branchLength = 0
        self.branchWidth = 0
        self.ctr = 0
        self.lastItNodeRigth = None
        self.lastItNodeLeft = None

        self.branchLengths = []
        self.branchWidths = []

        self.calcForNode = self.rootNode.getLeft()
        self.calculateHightReal(self.rootNode)
        return max(self.branchWidths)-min(self.branchWidths)

    def calculateHightRealRec(self,node):
        self.branchLength = self.branchLength + 1
        if(self.canGoRight(node)):     
            self.lastItNodeLeft = None
            self.lastItNodeRigth = node
            node = self.goRight(node)
            self.branchWidth = self.branchWidth + 1
            self.calculateHightRealRec(node)
        else:
            if(self.canGoLeft(node)):
                self.lastItNodeRigth = None
                self.lastItNodeLeft = node
                node = self.goLeft(node)
                self.branchWidth = self.branchWidth - 1
                self.calculateHightRealRec(node)
            else:
                self.branchLengths.append(self.branchLength)
                self.branchWidths.append(self.branchWidth)
                self.branchLength = 0
                self.branchWidth = 0
                if(node is not self.calcForNode):
                    if(self.lastItNodeRigth != None):
                        self.deletedVals.append(node.getVal())
                        self.lastItNodeRigth.setRight(None)
                    if(self.lastItNodeLeft != None):
                        self.deletedVals.append(node.getVal())
                        self.lastItNodeLeft.setLeft(None)

                    self.lastItNodeRigth = None
                    self.lastItNodeLeft = None
                    #calculate again
                    self.calculateHightRealRec(self.calcForNode)


    def calculateHightReal(self, node):
        self.calcForNode = node
        self.calculateHightRealRec(node)

    def __str__(self):
            fronta = []
            leva = []
            prava = []
            polovina = self.posMatrix.shape[1]
            polovina = polovina / 2
            for i in range(self.posMatrix.shape[0]):
                for j in range(self.posMatrix.shape[1]):
                    if(self.posMatrix[i][j] != math.inf):
                       fronta.append(self.posMatrix[i][j])
                       if(j <= polovina):
                            leva.append(self.posMatrix[i][j])
                       if(j >= polovina):
                            prava.append(self.posMatrix[i][j])
                    i = i - 1
                    j = j - 1
                    if(self.posMatrix[i][j] != math.inf and i > 0 and j >= polovina):
                        leva.append("-")
                    j = j + 2
                    if(j < self.posMatrix.shape[0]):
                        if(self.posMatrix[i][j] != math.inf and i > 0 and j >= polovina):
                            prava.append("-")    
                    i = i + 1
                    j = j - 2
                      
                          
            leva.pop(0)
            l = 0
            p = 0
            z = 0
            z1 = 0
            leftlenght = len(leva)
            rightlenght = len(prava)
            print(len(fronta))
            for i in range(len(fronta)):
                if(prava[p] == "-" and p<rightlenght):
                    p = p + 1 
                if(leva[l] == "-" and l<leftlenght-1):
                    l = l + 1 
                if(i == 0):
                    print(fronta[i], " (", leva[l], " ,", prava[p], " )", end= '')
                if(leva[l] == fronta[i]):
                    l = l + 1
                    z1 = l + 2
                    if(z1 > leftlenght):
                        print(fronta[i],"( - , - )", end='')
                    else:
                        print(fronta[i], " (", leva[l], " ,", leva[l + 1], " )", end= '')       
                if(prava[p] == fronta[i]):
                    p = p + 1
                    z = p + 2
                    if(z > rightlenght):
                        	print(fronta[i],"( - , - )", end='')
                    else:
                            print(fronta[i], " (", prava[p], " ,", prava[p + 1], " )", end= '')

                      
                    

          #  print(fronta[0], " (", leva[0], " ,", prava[0], " )", end= '')
          #  print(fronta[1], " (", leva[1], " ,", leva[2], " )", end= '')
          #  print(fronta[3], " (", leva[3], " ,", leva[4], " )", end= '')
          #  print(fronta[2], " (", prava[0], " ,", prava[1], " )", end= '')   
          #  print(fronta[4], " (", prava[2], " ,", prava[3], " )", end= '')                         
            return ""


    def contains(self,val):
        testNode = Node(None,None, val)
        return self.checkIfThere(self.rootNode,testNode)

n0 = Node(None,None,0)

tr = Tr(n0)
tr.addNode(1)
tr.addNode(-2)
tr.addNode(-3)
tr.addNode(3)
tr.addNode(2)
tr.addNode(4)
tr.addNode(-11)
  
tr.calculatePosMatrix()
print(tr) 
